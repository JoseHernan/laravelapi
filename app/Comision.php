<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comision extends Model
{
	protected $fillable = [
        'id',
        'facultad_id',
        'carrera_id',
        'materia_id',
        'catedra_id',
        'turno',
        'semestre',
        'anio'
    ];
}