<?php

namespace App\Http\Controllers;

use App\Comision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ComisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comisiones = DB::select('select comision.id, 
            count(alumno_comision.alumno_id) as cantidad_alumnos,
            facultad.nombre as nombre_facultad,
            carrera.nombre as nombre_carrera,
            materia.nombre as nombre_materia,
            comision.catedra_id as catedra,
            comision.turno,
            comision.semestre,
            comision.anio
        from comision
            join facultad on comision.facultad_id = facultad.id
            join carrera on comision.carrera_id = carrera.id
            join materia on comision.materia_id = materia.id
            left outer join alumno_comision on comision.id = alumno_comision.comision_id group by comision.id');

        return $comisiones;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comision  $Comision
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //return Comision::find($id);

        $comision = DB::select('select comision.id, 
            facultad.nombre as nombre_facultad,
            carrera.nombre as nombre_carrera,
            materia.nombre as nombre_materia,
            comision.catedra_id as catedra,
            comision.turno,
            comision.semestre,
            comision.anio
        from comision
            join facultad on comision.facultad_id = facultad.id
            join carrera on comision.carrera_id = carrera.id
            join materia on comision.materia_id = materia.id
            WHERE comision.id = ' . $id);

        return $comision;

    }

}
