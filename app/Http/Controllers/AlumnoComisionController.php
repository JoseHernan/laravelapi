<?php

namespace App\Http\Controllers;

use App\AlumnoComision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class AlumnoComisionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return [];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comision_id = $request->input('id');
        $alumnos = $request->input('alumnos');
        $dup = '' ; 

        foreach ($alumnos as $key => $alumno) {
            AlumnoComision::where('comision_id', $comision_id)
                    ->where('alumno_id', $alumno['id'])
                    ->update(['nota_final' => $alumno['notaFinal']]);
        }


        $i = 6; 
        return ['id' => $request->input('id'),  'cantidad' => $dup];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AlumnoComision  $alumnoComision
     * @return \Illuminate\Http\Response
     */
    public function show($comision_id)
    {
        $alumnos = DB::select('select alumno.id, alumno.nombre, alumno.apellido, alumno_comision.nota_final from alumno join alumno_comision on alumno.id = alumno_comision.alumno_id
            where alumno_comision.comision_id = ' . $comision_id );

        return $alumnos;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AlumnoComision  $alumnoComision
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AlumnoComision $alumnoComision)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AlumnoComision  $alumnoComision
     * @return \Illuminate\Http\Response
     */
    public function destroy(AlumnoComision $alumnoComision)
    {
        //
    }
}
