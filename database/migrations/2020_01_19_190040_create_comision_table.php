<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateComisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comision', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id');
            $table->bigInteger('facultad_id')->unsigned();
            $table->bigInteger('carrera_id')->unsigned();
            $table->bigInteger('materia_id')->unsigned();
            $table->char('catedra_id',1);
            $table->set('turno', ['M','T','N']);
            $table->set('semestre',['1','2']);
            $table->year('anio');
            $table->foreign('facultad_id', 'fk_facultad_id')->references('id')->on('facultad');
            $table->foreign('carrera_id', 'fk_carrera_id')->references('id')->on('carrera');
            $table->foreign('materia_id', 'fk_materia_id')->references('id')->on('materia');
            $table->foreign('catedra_id', 'fk_catedra_id')->references('id')->on('catedra');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comision');
    }
}
