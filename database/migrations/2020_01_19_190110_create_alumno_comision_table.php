<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnoComisionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumno_comision', function (Blueprint $table) {
            $table->bigInteger('alumno_id')->unsigned();
            $table->bigInteger('comision_id')->unsigned();
            $table->string('nota_final');
            $table->foreign('alumno_id', 'fk_alumno_id')->references('id')->on('alumno');
             $table->foreign('comision_id', 'fk_comision_id')->references('id')->on('comision');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumno_comision');
    }
}
